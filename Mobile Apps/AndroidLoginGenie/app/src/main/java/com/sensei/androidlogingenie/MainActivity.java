package com.sensei.androidlogingenie;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;
import com.dropbox.client2.session.TokenPair;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

import java.util.ArrayList;

public class MainActivity extends BaseDemoActivity implements View.OnClickListener {

    private LinearLayout container;
    private DropboxAPI dropboxApi;
    private boolean isUserLoggedIn;
    private Button dropboxloginBtn;
    private Button googleloginBtn;
    private Button listFilesBtn;

    private GoogleApiClient googleApi;

    private final static String DROPBOX_FILE_DIR = "/";
    private final static String DROPBOX_NAME = "dropbox_prefs";
    private final static String ACCESS_KEY = "h63f43ppdhqp7kn";
    private final static String ACCESS_SECRET = "hrm91wovxtjwq4x";
    private final static AccessType ACCESS_TYPE = AccessType.DROPBOX;

    private final int RESOLVE_CONNECTION_REQUEST_CODE = 73;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dropboxloginBtn = (Button) findViewById(R.id.dropboxloginBtn);
        dropboxloginBtn.setOnClickListener(this);
        googleloginBtn = (Button) findViewById(R.id.googleloginBtn);
        googleloginBtn.setOnClickListener(this);
        listFilesBtn = (Button) findViewById(R.id.listFilesBtn);
        listFilesBtn.setOnClickListener(this);
        container = (LinearLayout) findViewById(R.id.container_files);

        loggedIn(false);

        AppKeyPair appKeyPair = new AppKeyPair(ACCESS_KEY, ACCESS_SECRET);
        AndroidAuthSession session;

        SharedPreferences prefs = getSharedPreferences(DROPBOX_NAME, 0);
        String key = prefs.getString(ACCESS_KEY, null);
        String secret = prefs.getString(ACCESS_SECRET, null);

        if (key != null && secret != null) {
            AccessTokenPair token = new AccessTokenPair(key, secret);
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE, token);
        } else {
            session = new AndroidAuthSession(appKeyPair, ACCESS_TYPE);
        }

        dropboxApi = new DropboxAPI<AndroidAuthSession>(session);

//        googleApi = new GoogleApiClient.Builder(this)
//                .addApi(Drive.API)
//                .addScope(Drive.SCOPE_FILE)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();

//        GoogleAccountCredential credential = GoogleAccountCredential.usingOAuth2(this, DriveScopes.DRIVE);
//        credential.setSelectedAccountName(accountName);
//        Drive service = new Drive.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential).build();

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        showMessage("Google Drive Connected");
    }

    @Override
    protected void onResume() {
        super.onResume();   // google
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // dropbox
        AndroidAuthSession session = (AndroidAuthSession) dropboxApi.getSession();
        if (session.authenticationSuccessful()) {
            try {
                session.finishAuthentication();

                TokenPair tokens = session.getAccessTokenPair();
                SharedPreferences prefs = getSharedPreferences(DROPBOX_NAME, 0);
                Editor editor = prefs.edit();
                editor.putString(ACCESS_KEY, tokens.key);
                editor.putString(ACCESS_SECRET, tokens.secret);
                editor.commit();
                Toast.makeText(getApplicationContext(), "Dropbox Connected", Toast.LENGTH_LONG).show();
                loggedIn(true);
            } catch (IllegalStateException e) {
                Toast.makeText(this, "Error during Dropbox authentication",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private final Handler handler = new Handler() {
        public void handleMessage(Message message) {
            ArrayList<String> result = message.getData().getStringArrayList("data");

            for (String fileName: result) {
                TextView textView = new TextView(MainActivity.this);
                textView.setText(fileName);
                container.addView(textView);
            }
        }
    };



    @Override
    public void onClick(View v) {
        AndroidAuthSession dbSession = (AndroidAuthSession) dropboxApi.getSession();
        switch (v.getId()) {
            case R.id.dropboxloginBtn:
                if (isUserLoggedIn) {
                    dbSession.unlink();
                    loggedIn(false);
                } else
                    dbSession.startAuthentication(MainActivity.this);

                break;

            case R.id.googleloginBtn:
//                if(!isUserLoggedIn) super.onResume();
//                getGoogleApiClient().connect();
                break;

            case R.id.listFilesBtn:
                ListFiles listFiles = new ListFiles(dropboxApi, DROPBOX_FILE_DIR,
                        handler, googleApi);
                listFiles.execute();
                Drive.DriveApi.fetchDriveId(getGoogleApiClient(), "genie")
                        .setResultCallback(idCallback);
                DriveFolder folder = Drive.DriveApi.getRootFolder(getGoogleApiClient());
                processGoogleDriveFolder(folder);
                Query query = new Query.Builder()
                        .addFilter(Filters.eq(SearchableField.TITLE, "HelloWorld.java"))
                        .build();


                break;

            default:
                break;
        }
    }

    private void processGoogleDriveFolder(DriveFolder folder) {
        assert folder != null;
        PendingResult<DriveApi.MetadataBufferResult> children = folder.listChildren(getGoogleApiClient());
        children.setResultCallback(folderContentsResult);
    }

    public void loggedIn(boolean userLoggedIn) {
        isUserLoggedIn = userLoggedIn;
        listFilesBtn.setEnabled(userLoggedIn);
        listFilesBtn.setBackgroundColor(userLoggedIn ? Color.BLUE : Color.GRAY);
        dropboxloginBtn.setText(userLoggedIn ? "Logout" : "Log in");
    }

    final private ResultCallback<DriveApi.MetadataBufferResult> folderContentsResult = new
            ResultCallback<DriveApi.MetadataBufferResult>() {

                @Override
                public void onResult(DriveApi.MetadataBufferResult metadataBufferResult) {
                    if (!metadataBufferResult.getStatus().isSuccess()) {
                        showMessage("Problem while retrieving files");
                        return;
                    }
                    showMessage("something worked");
                    MetadataBuffer metadataBuffer = metadataBufferResult.getMetadataBuffer();
                    Metadata firstLine = metadataBuffer.get(0);
                    showMessage(firstLine.getTitle());
                }
            };


    final private ResultCallback<DriveApi.DriveIdResult> idCallback = new ResultCallback<DriveApi.DriveIdResult>() {
        @Override
        public void onResult(DriveApi.DriveIdResult result) {
            if (!result.getStatus().isSuccess()) {
                showMessage("Cannot find DriveId. Are you authorized to view this file?");
                return;
            }
            DriveFolder folder = Drive.DriveApi.getFolder(getGoogleApiClient(), result.getDriveId());
            folder.listChildren(getGoogleApiClient())
                    .setResultCallback(metadataResult);
        }
    };

    final private ResultCallback<DriveApi.MetadataBufferResult> metadataResult = new
            ResultCallback<DriveApi.MetadataBufferResult>() {
                @Override
                public void onResult(DriveApi.MetadataBufferResult result) {
                    if (!result.getStatus().isSuccess()) {
                        showMessage("Problem while retrieving files");
                        return;
                    }
//                    mResultsAdapter.clear();
//                    mResultsAdapter.append(result.getMetadataBuffer());
                    showMessage("Successfully listed files.");
                }
            };

}


/*
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi.MetadataBufferResult;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.widget.DataBufferAdapter;

*/
/**
 * An activity illustrates how to list file results and infinitely
 * populate the results list view with data if there are more results.
 *//*

public class MainActivity extends BaseDemoActivity {

    private ListView mListView;
    private DataBufferAdapter<Metadata> mResultsAdapter;
    private String mNextPageToken;
    private boolean mHasMore;

    @Override
    protected void onCreate(Bundle b) {
        super.onCreate(b);
        setContentView(R.layout.activity_main);

        mHasMore = true; // initial request assumes there are files results.

        mListView = (ListView) findViewById(R.id.listViewResults);
        mResultsAdapter = new ResultsAdapter(this);
        mListView.setAdapter(mResultsAdapter);
        mListView.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            */
/**
             * Handles onScroll to retrieve next pages of results
             * if there are more results items to display.
             *//*

            @Override
            public void onScroll(AbsListView view, int first, int visible, int total) {
                if (mNextPageToken != null && first + visible + 5 < total) {
                    retrieveNextPage();
                }
            }
        });
    }

    */
/**
     * Clears the result buffer to avoid memory leaks as soon
     * as the activity is no longer visible by the user.
     *//*

    @Override
    protected void onStop() {
        super.onStop();
        mResultsAdapter.clear();
    }

    */
/**
     * Handles the Drive service connection initialization
     * and inits the first listing request.
     *//*

    @Override
    public void onConnected(Bundle connectionHint) {
        super.onConnected(connectionHint);
        retrieveNextPage();
    }

    */
/**
     * Retrieves results for the next page. For the first run,
     * it retrieves results for the first page.
     *//*

    private void retrieveNextPage() {
        // if there are no more results to retrieve,
        // return silently.
        if (!mHasMore) {
            return;
        }
        // retrieve the results for the next page.
        Query query = new Query.Builder()
                .setPageToken(mNextPageToken)
                .build();
        Drive.DriveApi.query(getGoogleApiClient(), query)
                .setResultCallback(metadataBufferCallback);
    }

    */
/**
     * Appends the retrieved results to the result buffer.
     *//*

    private final ResultCallback<MetadataBufferResult> metadataBufferCallback = new
            ResultCallback<MetadataBufferResult>() {
                @Override
                public void onResult(MetadataBufferResult result) {
                    if (!result.getStatus().isSuccess()) {
                        showMessage("Problem while retrieving files");
                        return;
                    }
                    mResultsAdapter.append(result.getMetadataBuffer());
                    mNextPageToken = result.getMetadataBuffer().getNextPageToken();
                    mHasMore = mNextPageToken != null;
                }
            };
}*/
