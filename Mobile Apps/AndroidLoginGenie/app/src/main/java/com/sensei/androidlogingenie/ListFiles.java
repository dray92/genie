package com.sensei.androidlogingenie;

/**
 * Created by Debosmit on 7/9/15.
 */

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.exception.DropboxException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.widget.DataBufferAdapter;

import java.util.ArrayList;

public class ListFiles extends AsyncTask<Void, Void, ArrayList> {

    private DropboxAPI dropboxApi;
    private String path;
    private Handler handler;

    private GoogleApiClient googleApi;
    private boolean googleHasmore;
    private DataBufferAdapter<Metadata> mResultsAdapter;
    private String googleNextPageToken;

    private String[] imageFileTypes = {"tif", "jpeg", "jpg", "png", "gif"};

    public ListFiles(DropboxAPI dropboxApi, String path, Handler handler, GoogleApiClient googleApi) {
        this.dropboxApi = dropboxApi;
        this.path = path;
        this.handler = handler;
        this.googleApi = googleApi;
        this.googleHasmore = true;  // assume there are files in Google drive
    }

    @Override
    protected ArrayList doInBackground(Void... params) {
        ArrayList files = new ArrayList();
        try {
            getFilenames(path, files);
        } catch (DropboxException e) {
            e.printStackTrace();
        }
        return files;
    }

    private ArrayList getFilenames(String curPath, ArrayList files) throws DropboxException {
        Entry directory = dropboxApi.metadata(curPath, 1000, null, true, null);
        if(directory == null) return null;
        if(directory.contents.size() == 0) return null;
        for (Entry entry : directory.contents) {
            if(isDirectory(entry.fileName()))
                getFilenames((curPath+entry.fileName()+"/").replace("//","/"), files);
            else {
                files.add("DROPBOX:: "+curPath + entry.fileName());
            }
        }
        return null;
    }

    // returns false if is a file ending with
    private boolean isDirectory(String filename) {
        for(String fileType: imageFileTypes) {
            if (filename.endsWith("." + fileType))
                return false;
        }
        return true;
    }

    @Override
    protected void onPostExecute(ArrayList result) {
        Message message = handler.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("data", result);
        message.setData(bundle);
        handler.sendMessage(message);
    }
}
