package com.prgguru.example;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.Calendar;

@SuppressLint("NewApi")
public class MainActivity extends Activity {
	ProgressDialog prgDialog;
	String encodedString;
	RequestParams params = new RequestParams();
	String imgPath, fileName;
	TextView status;
	Bitmap bitmap;
	private static int RESULT_LOAD_IMG = 1;
	public Socket socket;
	private AsyncTask currentTask;
	private Thread t;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		prgDialog = new ProgressDialog(this);
		// Set Cancelable as False
		prgDialog.setCancelable(false);
		status = (TextView) findViewById(R.id.status);
	}

	public void loadImagefromGallery(View view) {
		// Create intent to Open Image applications like Gallery, Google Photos
		Intent galleryIntent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		// Start the Intent
		startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
	}

	// When Image is selected from Gallery
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		try {
			// When an Image is picked
			if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
					&& null != data) {
				// Get the Image from data

				Uri selectedImage = data.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				// Get the cursor
				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				// Move to first row
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				imgPath = cursor.getString(columnIndex);
				cursor.close();
				ImageView imgView = (ImageView) findViewById(R.id.imgView);
				// Set the Image in ImageView
				imgView.setImageBitmap(BitmapFactory
						.decodeFile(imgPath));
				// Get the Image's file name
				String fileNameSegments[] = imgPath.split("/");
				fileName = fileNameSegments[fileNameSegments.length - 1];
				// Put file name in Async Http Post Param which will used in Php web app
				params.put("filename", fileName);
				status.setText("");
			} else {
				Toast.makeText(this, "You haven't picked Image",
						Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
					.show();
		}

	}

	// When Upload button is clicked
	public void uploadImage(View v) throws IOException {
		// When Image is selected from Gallery
		if (imgPath != null && !imgPath.isEmpty()) {
			prgDialog.setMessage("Converting Image to Binary Data");
			prgDialog.show();
			ExifInterface exif = new ExifInterface(imgPath);
			String data = exif.getAttribute(ExifInterface.TAG_DATETIME);
			// Convert image to String using Base64

			t = new Thread(new ClientThread());
			t.start();	// start socket thread

			encodeImagetoString();
		// When Image is not selected from Gallery
		} else {
			Toast.makeText(
					getApplicationContext(),
					"You must select image from gallery before you try to upload",
					Toast.LENGTH_LONG).show();
		}
	}

	// AsyncTask - To convert Image to String
	public void encodeImagetoString() {
		new AsyncTask<Void, Void, String>() {

			protected void onPreExecute() {

			};

			@Override
			protected String doInBackground(Void... params) {
				BitmapFactory.Options options = null;
				options = new BitmapFactory.Options();
				options.inSampleSize = 3;
				bitmap = BitmapFactory.decodeFile(imgPath,
						options);
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				// Must compress the Image to reduce image size to make upload easy
				bitmap.compress(Bitmap.CompressFormat.PNG, 50, stream);
				byte[] byte_arr = stream.toByteArray();
				// Encode Image to String
				encodedString = Base64.encodeToString(byte_arr, 0);
				return "";
			}

			@Override
			protected void onPostExecute(String msg) {
				prgDialog.setMessage("Calling Upload");
				// Put converted Image string into Async Http Post param
				params.put("image", encodedString);
				// Trigger Image upload
				triggerImageUpload();
			}
		}.execute(null, null, null);
	}

	public void triggerImageUpload() {
//		currentTask = new ConnectionTask();
		new ConnectionTask().execute();
//		doSocketConnection();
//		makeHTTPCall();
	}

	public void filenameSent() throws InterruptedException {
		Display("Image name Sent");
//		prgDialog.show();
//		prgDialog.setMessage("Sending Image");
//		Thread.sleep(1000);
//		prgDialog.hide();
		new ImageSendTask().execute();
	}



	private void doSocketConnection() {
		Thread t = new Thread(new Runnable(){

			@Override
			public void run() {
				try {
					Socket s = new Socket("128.95.66.179", 10000); //create socket

					// Write to socket
//					ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream()); //create the object to be sent
//					byte[] buffer1 = "Proceed".getBytes(Charset.forName("UTF-8"));
//					oos.writeObject(buffer1); //send the contents of the buffer to the socket
//					oos.flush();
//					oos.close();
					String myString = "proceed=image.jpg";
//					FileInputStream fis = new FileInputStream("Proceed"); //read image as bytes
					byte [] buffer1 = myString.getBytes(Charset.forName("UTF-8")); //create buffer that will hold the bytes
//					fis.read(buffer1); //read file contents to the buffer
//
//					// Write to socket
					ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream()); //create the object to be sent
					oos.writeObject(buffer1); //send the contents of the buffer to the socket
					oos.flush();
					oos.close();
					Log.d("Server", "Done sending message");
					// Read from socket
					BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
					StringBuilder response = new StringBuilder();
					String line;
					while ((line = input.readLine()) != null)
						response.append(line);
					Message clientmessage = Message.obtain();
					clientmessage.obj = response.toString();
					mHandler.sendMessage(clientmessage);
					input.close();
					s.close();
					prgDialog.show();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} //catch (ClassNotFoundException e) {
				//e.printStackTrace();
				//}
			}

		});

		t.start();

	}

	Handler mHandler = new Handler(){
		@Override
		public void handleMessage(Message msg){
			Display(msg.toString());
		}
	};

	public void Display(String response) {
		Toast.makeText(getApplicationContext(), response,
				Toast.LENGTH_SHORT).show();
	}

	// http://192.168.2.4:9000/imgupload/upload_image.php
	// http://192.168.2.4:9999/ImageUploadWebApp/uploadimg.jsp
	// Make Http call to upload Image to Php server
	public void makeHTTPCall() {
		prgDialog.setMessage("Processing Image");
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(18000);
		final Calendar c = Calendar.getInstance();
		String starttime = c.get(Calendar.HOUR) + ":"
				+ c.get(Calendar.MINUTE) + ":"
				+ c.get(Calendar.SECOND) + ":"
				+ c.get(Calendar.MILLISECOND);
		Log.d("Start", "Time: " + starttime );

		// Don't forget to change the IP address to your LAN address. Port no as well.
		client.post("http://192.168.0.2/~Debosmit/genie/upload_image.php", params, new AsyncHttpResponseHandler() {

			// When the response returned by REST has Http
			// response code '200'
			@Override
			public void onSuccess(String response) {
				// Hide Progress Dialog
				prgDialog.hide();
				Toast.makeText(getApplicationContext(), response,
						Toast.LENGTH_LONG).show();
				status.setText(response);
				String endtime = c.get(Calendar.HOUR) + ":"
						+ c.get(Calendar.MINUTE) + ":"
						+ c.get(Calendar.SECOND) + ":"
						+ c.get(Calendar.MILLISECOND);
				Log.d("SUCCESS", "Time: " + endtime);
				Log.d("SUCCESS", response);
			}

			// When the response returned by REST has Http
			// response code other than '200' such as '404',
			// '500' or '403' etc
			@Override
			public void onFailure(int statusCode, Throwable error,
								  String content) {
				// Hide Progress Dialog
				prgDialog.hide();
				// When Http response code is '404'
				if (statusCode == 404) {
					Toast.makeText(getApplicationContext(),
							"Requested resource not found",
							Toast.LENGTH_LONG).show();
				}
				// When Http response code is '500'
				else if (statusCode == 500) {
					Toast.makeText(getApplicationContext(),
							"Something went wrong at server end",
							Toast.LENGTH_LONG).show();
				}
				// When Http response code other than 404, 500
				else {
					Toast.makeText(
							getApplicationContext(),
							"Error Occured \n Most Common Error: \n1. Device not connected to Internet\n2. Web App is not deployed in App server\n3. App server is not running\n HTTP Status code : "
									+ statusCode, Toast.LENGTH_LONG)
							.show();
				}
				status.setText("FAILURE: " + statusCode);
				Log.d("FAILURE", statusCode + content);
			}

			@Override
			public void onFinish() {
				// Hide Progress Dialog
				prgDialog.hide();
				String endtime = c.get(Calendar.HOUR) + ":"
						+ c.get(Calendar.MINUTE) + ":"
						+ c.get(Calendar.SECOND) + ":"
						+ c.get(Calendar.MILLISECOND);
				Log.d("Finish", "Time: " + endtime);
//						status.setText("Finished at: " + c.get(Calendar.SECOND));
					}
				});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// Dismiss the progress bar when application is closed
		if (prgDialog != null) {
			prgDialog.dismiss();
		}
	}

	class ConnectionTask extends AsyncTask<String, Void, String> {
		final String TAG = "Server";
		protected String doInBackground(String... params) {
			String responce = null;

			try {
				String str = "proceed=image.jpg";
				PrintWriter out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream())), true);
				out.println(str);
				out.flush();

				Log.d(TAG, "message sent");
				InputStream input = socket.getInputStream();
				int lockSeconds = 20*1000;
				Log.d(TAG, "input stream opened");
				long lockThreadCheckpoint = System.currentTimeMillis();
				int availableBytes = input.available();
				while(availableBytes <=0 && (System.currentTimeMillis() < lockThreadCheckpoint + lockSeconds)){
					try{Thread.sleep(10);}catch(InterruptedException ie){ie.printStackTrace();}
					availableBytes = input.available();
				}

				byte[] buffer = new byte[availableBytes];
				input.read(buffer, 0, availableBytes);
				responce = new String(buffer);
				Log.d(TAG, "Thread status: " + t.getState());
				Log.d(TAG, "Thread is alive? " + t.isAlive());
//				out.close();
//				input.close();
//				socket.close();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				socket.setKeepAlive(true);
			} catch (SocketException e) {
				e.printStackTrace();
			}
			return responce;
		}
		protected void onPostExecute(String responce) {
			prgDialog.hide();
			Log.d(TAG, "Respnse: " + responce);
			Display(responce);
			try {
				filenameSent();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}


//			if(responce.equalsIgnoreCase("ready"))
//				new ImageSendTask().execute();
		}
	}

	class ImageSendTask extends AsyncTask<String, Void, String> {
		final String TAG = "ImageSend";

		protected void onPreExecute() {
			prgDialog.show();
			prgDialog.setMessage("Inside ImageSendTask");
		}

		protected String doInBackground(String... params) {
			String responce = null;
			String EOF = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
			try {
				Log.d(TAG, "Is socket connected? " + socket.isConnected());
				Log.d(TAG, "Is socket closed? " + socket.isClosed());
//				Log.d(TAG, "Thread status: " + t.getState());
//				Log.d(TAG, "Thread is alive? " + t.isAlive());

				String str = encodedString;
				PrintWriter out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(socket.getOutputStream())), true);
//				out.println(str);
//				out.flush();
				out.println(str);
				out.flush();
				out.println(EOF);
				out.flush();
				Log.d(TAG, "image sent");
				InputStream input = socket.getInputStream();
				int lockSeconds = 15*1000;
				Log.d(TAG, "input stream opened");
				long lockThreadCheckpoint = System.currentTimeMillis();
				int availableBytes = input.available();
				while(availableBytes <=0 && (System.currentTimeMillis() < lockThreadCheckpoint + lockSeconds)){
					try{Thread.sleep(10);}catch(InterruptedException ie){ie.printStackTrace();}
					availableBytes = input.available();
				}

				byte[] buffer = new byte[availableBytes];
				input.read(buffer, 0, availableBytes);
				responce = new String(buffer);

				out.close();
				input.close();
				socket.close();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return responce;
		}
		protected void onPostExecute(String responce) {
			prgDialog.hide();
			Log.d(TAG, "Respnse: " + responce);
			Display("Respnse: " + responce);

		}
	}

	class ClientThread implements Runnable {

		@Override
		public void run() {

			try {
				InetAddress serverAddr = InetAddress.getByName("140.142.161.212");

				socket = new Socket(serverAddr, 10000);
				socket.setKeepAlive(true);

			} catch (UnknownHostException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

		}

	}
}