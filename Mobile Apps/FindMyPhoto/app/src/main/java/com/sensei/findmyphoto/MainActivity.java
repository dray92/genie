package com.sensei.findmyphoto;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity {
    private GridView gridView;
    private GridViewAdapter gridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, getData());
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
            ImageItem item = (ImageItem) parent.getItemAtPosition(position);

            //Create intent
            Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
            intent.putExtra("title", item.getTitle());
                Bitmap myImage = item.getImage();
                myImage = scaleDownBitmap(myImage, 100);
            intent.putExtra("image", myImage);

            //Start details activity
            startActivity(intent);
            }
        });
    }

    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight) {
        DisplayMetrics metrics = new DisplayMetrics();

//        final float densityMultiplier = context.getResources().getDisplayMetrics().density;
        final float densityMultiplier = metrics.DENSITY_XXHIGH / 160;
        int h= (int) (newHeight*densityMultiplier);
        int w= (int) (h * photo.getWidth()/((double) photo.getHeight()));

        photo=Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }

    /**
     * Prepare some dummy data for gridview
     */
    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            imageItems.add(new ImageItem(bitmap, "Image#" + i));
        }
        ArrayList<ImageItem> phoneImages = new ArrayList<>();

        String[] projection = {MediaStore.Images.Thumbnails._ID};
//        Cursor cursor = managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                projection, // Which columns to return
//                null,       // Return all rows
//                null,
//                MediaStore.Images.Thumbnails.IMAGE_ID);
//        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
        return imageItems;
    }
}