# !/usr/bin/python
# coding:utf-8
import socket
import sys
import time
import base64


ip = '127.0.0.1'
port = 60000
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


def recvfile(filename):
    print "server ready, now client rece file~~"
    f = open(filename, 'wb')
    while True:
        data = s.recv(4096)
        if data == 'EOF':
            print "recv file success!"
            break
        f.write(data)
    f.close()


def sendfile(filename):
    print "server ready, now client sending file~~"
    with open(filename, "rb") as f:
        encoded_string = base64.b64encode(f.read())
    f.close()

    s.sendall(encoded_string)
    time.sleep(1)
    s.sendall(base64.b64encode('EOF'))
    print "send file success!"


def confirm(s, client_command):
    s.send(client_command)
    data = s.recv(4096)
    if data == 'ready':
        return True

try:
    s.connect((ip, port))
    while True:
        client_command = raw_input(">>")
        if not client_command:
            continue
        action, filename = client_command.split()
        if action == 'put':
            if confirm(s, client_command):
                sendfile("./images_example/" + filename)
            else:
                print "server get error!"
        elif action == 'get':
            if confirm(s, client_command):
                recvfile("./images_example/" + filename)
            else:
                print "server get error!"
        elif action == 'proceed':
            if confirm(s, client_command):
                sendfile("./images_example/" + filename)
                print "processing images."
                recvfile("./images_example/result.txt")
            else:
                print "server get error!"
        else:
            print "command error!"
except socket.error as e:
    print "get error as", e
finally:
    s.close()
