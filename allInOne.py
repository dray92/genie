import sys
sys.path.append("/Users/Debosmit/Sites/genie/manager/caffe/python")
import caffe
import base64
import numpy as np
import argparse
import os
import time
import scipy.io
import pdb
import shutil
import json
import cPickle as pickle
# import code
# import math
from imagernn.solver import Solver
from imagernn.imagernn_utils import decodeGenerator, eval_split
import SocketServer
# import subprocess
# import string


class MyTcpServer(SocketServer.BaseRequestHandler):

    def recvfile(self, filename):
        EOF = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        print "starting receive file!"
        f = open(filename.strip(), 'wb')
        bufferImg = '';
        self.request.send('ready')
        while True:
            data = self.request.recv(4096);
            if EOF not in data:
                bufferImg += data;
                continue;
            else:
                break;
        f.write(base64.b64decode(bufferImg.strip()))    
        f.close()
        print "Successfully received file"

    def sendfile(self, filename):
        print "starting send file!"
        self.request.send('ready')
        time.sleep(1)
        f = open(filename, 'rb')
        while True:
            data = f.read(4096)
            if not data:
                break
            self.request.sendall(data)
        f.close()
        time.sleep(1)
        self.request.sendall('EOF')
        print "send file success!"

    def sendresult(self, text):
        print "staring send result!"
        time.sleep(1)
        self.request.sendall(text.encode('utf8'))
        time.sleep(1)
        self.request.sendall('EOF')
        print "send result success!"

    def handle(self):
        print "get connection from :", self.client_address
        while True:
            try:
                data = self.request.recv(4096)
                print "get request:", data
                if not data:
                    print "break the connection!"
                    break
                else:
                    action, filename = data.split('=')
                    print "action: ", action
                    print "filename: ", filename
                    if action == "put":
                        self.recvfile("./file_transfer/" + filename)
                    elif action == 'get':
                        self.sendfile("./file_transfer/" + filename)
                    elif action == "proceed":
                        self.recvfile("./file_transfer/" + filename)
                        text = proceed()
                        self.request.send(text)
                    else:
                        print "get error!"
                        continue
            except Exception as e:
                print "get error at:", e


def reduce_along_dim(img, dim, weights, indicies):
    '''
    Perform bilinear interpolation given along the image dimension dim
    -weights are the kernel weights
    -indicies are the crossponding indicies location
    return img resize along dimension dim
    '''
    other_dim = abs(dim - 1)
    if other_dim == 0:  # resizing image width
        weights = np.tile(
            weights[
                np.newaxis,
                :,
                :,
                np.newaxis],
            (img.shape[other_dim],
             1,
             1,
             3))
        out_img = img[:, indicies, :] * weights
        out_img = np.sum(out_img, axis=2)
    else:   # resize image height
        weights = np.tile(
            weights[
                :,
                :,
                np.newaxis,
                np.newaxis],
            (1,
             1,
             img.shape[other_dim],
             3))
        out_img = img[indicies, :, :] * weights
        out_img = np.sum(out_img, axis=1)
    return out_img


def cubic_spline(x):
    '''
    Compute the kernel weights
    See Keys, "Cubic Convolution Interpolation for Digital Image
    Processing, " IEEE Transactions on Acoustics, Speech, and Signal
    Processing, Vol. ASSP-29, No. 6, December 1981, p. 1155.
    '''
    absx = np.abs(x)
    absx2 = absx**2
    absx3 = absx**3
    kernel_weight = (1.5 * absx3 - 2.5 * absx2 + 1) * (absx <= 1) + \
        (-0.5 * absx3 + 2.5 * absx2 - 4 * absx + 2) * ((1 < absx) & (absx <= 2))
    return kernel_weight


def contribution(in_dim_len, out_dim_len, scale):
    '''
    Compute the weights and indicies of the pixels involved in the cubic interpolation along each dimension.

    output:
    weights a list of size 2 (one set of weights for each dimension). Each item is of size OUT_DIM_LEN*Kernel_Width
    indicies a list of size 2(one set of pixel indicies for each dimension) Each item is of size OUT_DIM_LEN*kernel_width

    note that if the entire column weights is zero, it gets deleted since those pixels don't contribute to anything
    '''
    kernel_width = 4
    if scale < 1:
        kernel_width = 4 / scale

    x_out = np.array(range(1, out_dim_len + 1))
    # project to the input space dimension
    u = x_out / scale + 0.5 * (1 - 1 / scale)

    # position of the left most pixel in each calculation
    l = np.floor(u - kernel_width / 2)

    # maxium number of pixels in each computation
    p = int(np.ceil(kernel_width) + 2)

    indicies = np.zeros((l.shape[0], p), dtype=int)
    indicies[:, 0] = l

    for i in range(1, p):
        indicies[:, i] = indicies[:, i - 1] + 1

    # compute the weights of the vectors
    u = u.reshape((u.shape[0], 1))
    u = np.repeat(u, p, axis=1)

    if scale < 1:
        weights = scale * cubic_spline(scale * (indicies - u))
    else:
        weights = cubic_spline((indicies - u))

    weights_sums = np.sum(weights, 1)
    weights = weights / weights_sums[:, np.newaxis]

    indicies = indicies - 1
    indicies[indicies < 0] = 0
    # clamping the indicies at the ends
    indicies[indicies > in_dim_len - 1] = in_dim_len - 1

    valid_cols = np.all(
        weights == 0,
        axis=0) == False  # find columns that are not all zeros

    indicies = indicies[:, valid_cols]
    weights = weights[:, valid_cols]

    return weights, indicies


def imresize(img, cropped_width, cropped_height):
    '''
    Function implementing matlab's imresize functionality default behaviour
    Cubic spline interpolation with antialiasing correction when scaling down the image.

    '''

    width_scale = float(cropped_width) / img.shape[1]
    height_scale = float(cropped_height) / img.shape[0]

    if len(img.shape) == 2:  # Gray Scale Case
        img = np.tile(img[:, :, np.newaxis], (1, 1, 3))  # Broadcast

    order = np.argsort([height_scale, width_scale])
    scale = [height_scale, width_scale]
    out_dim = [cropped_height, cropped_width]

    weights = [0, 0]
    indicies = [0, 0]

    for i in range(0, 2):
        weights[i], indicies[i] = contribution(
            img.shape[i], out_dim[i], scale[i])

    for i in range(0, len(order)):
        img = reduce_along_dim(
            img, order[i], weights[
                order[i]], indicies[
                order[i]])

    return img


def preprocess_image(img):
    '''
    Preprocess an input image before processing by the caffe module.


    Preprocessing include:
    -----------------------
    1- Converting image to single precision data type
    2- Resizing the input image to cropped_dimensions used in extract_features() matlab script
    3- Reorder color Channel, RGB->BGR
    4- Convert color scale from 0-1 to 0-255 range (actually because image type is a float the
        actual range could be negative or >255 during the cubic spline interpolation for image resize.
    5- Subtract the VGG dataset mean.
    6- Reorder the image to standard caffe input dimension order ( 3xHxW)
    '''
    img = img.astype(np.float32)
    img = imresize(img, 224, 224)  # cropping the image
    img = img[:, :, [2, 1, 0]]  # RGB-BGR
    img = img * 255

    mean = np.array([103.939, 116.779, 123.68])  # mean of the vgg

    for i in range(0, 3):
        img[:, :, i] = img[:, :, i] - mean[i]  # subtracting the mean
    img = np.transpose(img, [2, 0, 1])
    return img  # HxWx3


def proceed():

    imgs = [f for f in os.listdir(input_directory) if f.endswith('.jpg')]
    path_imgs = [os.path.join(input_directory, file) for file in imgs]
    feats = np.zeros((4096, len(path_imgs)))

    for b in range(0, len(path_imgs), beam_size):
        list_imgs = []
        for i in range(b, b + beam_size):
            if i < len(path_imgs):
                list_imgs.append(
                    np.array(
                        caffe.io.load_image(
                            path_imgs[i])))  # loading images HxWx3 (RGB)
            else:
                # Appending the last image in order to have a batch of size 10.
                # The extra predictions are removed later..
                list_imgs.append(list_imgs[-1])

        # preprocess the images
        caffe_input = np.asarray([preprocess_image(in_) for in_ in list_imgs])
        predictions = caffe_net.forward(data=caffe_input)
        predictions = predictions[caffe_net.outputs[0]].transpose()

        if i < len(path_imgs):
            feats[:, b:i + 1] = predictions
            n = i + 1
        else:
            n = min(beam_size, len(path_imgs) - b)
            # Removing extra predictions, due to the extra last image
            # appending.
            feats[:, b:b + n] = predictions[:, 0:n]
            n += b
        print "%d out of %d done....." % (n, len(path_imgs))

    # output blob which we will dump to JSON for visualizing the results
    blob = {}
    blob['params'] = params
    blob['checkpoint_params'] = checkpoint_params
    blob['imgblobs'] = []

    # load the features for all images
    D, N = feats.shape

    # iterate over all images and predict sentences
    BatchGenerator = decodeGenerator(checkpoint_params)
    for n in xrange(N):
        print 'image %d/%d:' % (n, N)

        # encode the image
        img = {}
        img['feat'] = feats[:, n]
        img['local_file_path'] = path_imgs[n]

        # perform the work. heavy lifting happens inside
        kwparams = {'beam_size': beam_size}
        Ys = BatchGenerator.predict(
            [{'image': img}], model, checkpoint_params, **kwparams)

        # build up the output
        img_blob = {}
        img_blob['img_path'] = img['local_file_path']

        # encode the top prediction
        # take predictions for the first (and only) image we passed in
        top_predictions = Ys[0]
        # these are sorted with highest on top
        top_prediction = top_predictions[0]
        # ix 0 is the END token, skip that
        candidate = ' '.join([ixtoword[ix]
                              for ix in top_prediction[1] if ix > 0])
        print 'PRED: (%f) %s' % (top_prediction[0], candidate)
        img_blob['candidate'] = {
            'text': candidate,
            'logprob': top_prediction[0]}
        blob['imgblobs'].append(img_blob)

    # dump result struct to file
    # save_file = os.path.join(out_directory, 'result_struct.json')
    # print 'writing predictions to %s...' % (save_file, )
    # json.dump(blob, open(save_file, 'w'))

    # dump output html
    html = ''
    for img in blob['imgblobs']:
        html += '<img src="%s" height="400"><br>' % (img['img_path'], )
        html += '(%f) %s <br><br>' % (img['candidate']
                                      ['logprob'], img['candidate']['text'])
    html_file = os.path.join(out_directory, 'result.html')
    print 'writing html result file to %s...' % (html_file, )
    open(html_file, 'w').write(html)

    result = blob['imgblobs'][0]['candidate']['text']
    logprob = blob['imgblobs'][0]['candidate']['logprob']
    retString = "Result: " + result + " (logprob: " + '%.2f' % logprob + ")"
    return retString


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model_def',
        dest='model_def',
        default='/Users/Tao/workspace/caffe/models/vgg_ilsvrc_16/VGG_ILSVRC_16_layers.prototxt',
        type=str,
        help='Path to the VGG_ILSVRC_16_layers model definition file.')
    parser.add_argument(
        '--model',
        dest='model',
        default='/Users/Tao/workspace/caffe/models/vgg_ilsvrc_16/VGG_ILSVRC_16_layers.caffemodel',
        type=str,
        help='Path to VGG_ILSVRC_16_layers pretrained model weight file i.e VGG_ILSVRC_16_layers.caffemodel')
    parser.add_argument(
        '-i',
        dest='input_directory',
        default='/Users/Tao/workspace/Genie/file_transfer',
        help='Path to Directory containing images to be processed.')
    parser.add_argument(
        '--WITH_GPU',
        action='store_true',
        dest='WITH_GPU',
        help='Caffe uses GPU for feature extraction')
    parser.add_argument(
        '-o',
        dest='out_directory',
        default='/Users/Tao/workspace/Genie/file_transfer',
        help='Output directory to store the generated features')
    parser.add_argument(
        '-c',
        '--checkpoint',
        default='checkpoint_coco_11.14.p',
        type=str,
        help='the input checkpoint')
    parser.add_argument(
        '-b',
        '--beam_size',
        type=int,
        default=10,
        help='beam size in inference. 1 indicates greedy per-word max procedure. Good value is approx 20 or so, and more = better.')
    args = parser.parse_args()
    params = vars(args)  # convert to ordinary dict
    print json.dumps(params, indent=2)

    # Initialization
    model_def = params['model_def']
    model_file = params['model']
    WITH_GPU = params['WITH_GPU']
    check_point = params['checkpoint']
    input_directory = params['input_directory']
    out_directory = params['out_directory']
    beam_size = params['beam_size']

    if WITH_GPU:
        caffe.set_mode_gpu()
    else:
        caffe.set_mode_cpu()
    print "loading model:", model_file
    caffe_net = caffe.Classifier(
        model_def,
        model_file,
        image_dims=(
            224,
            224),
        raw_scale=255,
        channel_swap=(
            2,
            1,
            0),
        mean=np.array(
            [
                103.939,
                116.779,
                123.68]))

    # load the checkpoint
    print 'loading checkpoint %s' % (check_point, )
    checkpoint = pickle.load(open(check_point, 'rb'))
    checkpoint_params = checkpoint['params']
    # dataset = checkpoint_params['dataset']
    model = checkpoint['model']
    # misc = {}
    # misc['wordtoix'] = checkpoint['wordtoix']
    ixtoword = checkpoint['ixtoword']

    # Clean files in input_directory
    for the_file in os.listdir(input_directory):
        file_path = os.path.join(input_directory, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            # elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print e

    print 'Initialization done!'

    host = '140.142.161.212'
    port = 10000
    s = SocketServer.ThreadingTCPServer((host, port), MyTcpServer)
    s.serve_forever()
