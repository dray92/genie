import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Bind the socket to the port
server_address = ('128.95.66.179', 10000)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
# Listen for incoming connections
sock.listen(1)

while True:
        # Wait for a connection
        print >>sys.stderr, 'waiting for a connection'
        connection, client_address = sock.accept()
        try:
                print >>sys.stderr, 'connection from', client_address

                # Receive the data
                while True:
                        data = connection.recv(1024)
                        if data.lower() != 'q':
                                print "<-- client: ", data
                        else:
                                print "Quit from Client"
                                connection.close()
                                break
                        data = raw_input("--> server: ")
                        if data.lower() != 'q':
                                connection.sendall(data)
                        else:
                                print "Quit from Server"
                                connection.close()
                                break

        finally:
                # Clean up the connection
                print "Connection close"
                connection.close()