<?php
    // Get image string posted from Android App
    $base=$_REQUEST['image'];
    // Get file name posted from Android App
    $filename = $_REQUEST['filename'];
    // Decode Image
    $binary=base64_decode($base);
    putenv('PATH=$PATH:/Users/Debosmit/anaconda/bin/python2.7');
    putenv('PATH=$PATH:/Developer/NVIDIA/CUDA-7.0/bin:/usr/local/bin');
    putenv('DYLD_LIBRARY_PATH=/Developer/NVIDIA/CUDA-7.0/lib:$DYLD_LIBRARY_PATH');
    putenv('DYLD_FALLBACK_LIBRARY_PATH=/Users/Debosmit/anaconda/lib:/usr/local/lib:/usr/lib:/opt/intel/lib:/opt/intel/mkl/lib');
    
    exec ("find manager -type d -exec chmod 0777 {} +");
    exec ("find manager -type f -exec chmod 0777 {} +");
header('Content-Type: bitmap; charset=utf-8');
    $filepath = 'manager/images_example';

    $files = glob($filepath.'/*.jpg'); // get all file names
    foreach($files as $file) { // iterate files
        if(is_file($file))
            unlink($file); // delete file
    }

    // Images will be saved under 'www/imgupload/uplodedimages' folder

    $file = fopen($filepath.'/'.$filename, 'wb');

    // $output = shell_exec('./script.sh');
    // Create File
    fwrite($file, $binary);
    fclose($file);

    // $command = "/Users/Debosmit/anaconda/bin/python manager/py_caffe_feat_extract_integrated.py --model_def_path /Users/Debosmit/Sites/genie/manager/caffe/models/vgg_ilsvrc_16 --model_path /Users/Debosmit/Sites/genie/manager/caffe/models/vgg_ilsvrc_16 -i /Users/Debosmit/Sites/genie/manager/images_example -o /Users/Debosmit/Sites/genie/manager/images_example";

    // $command = "./script.sh; echo 'tasks.txt done'";
    // exec($command, $output);
    
    $output = file_put_contents($filepath.'/'.'tasks.txt', "$filename");

    $command1 = "/Users/Debosmit/anaconda/bin/python manager/py_caffe_feat_extract_integrated.py --model_def_path /Users/Debosmit/Sites/genie/manager/caffe/models/vgg_ilsvrc_16 --model_path /Users/Debosmit/Sites/genie/manager/caffe/models/vgg_ilsvrc_16 -i /Users/Debosmit/Sites/genie/manager/images_example -o /Users/Debosmit/Sites/genie/manager/images_example";
    exec($command1, $output1);

    $filename = $filepath.'/'.'LOG1.txt';
    file_put_contents($filename, print_r($output1, true));

    $command1 = "/Users/Debosmit/anaconda/bin/python manager/predict_on_images.py -c /Users/Debosmit/Sites/genie/manager/checkpoint_coco_11.14.p -r /Users/Debosmit/Sites/genie/manager/images_example/";
    exec($command1, $output2);

    $filename = $filepath.'/'.'LOG2.txt';
    file_put_contents($filename, print_r($output2, true));

    $string = file_get_contents("manager/images_example/result_struct.json");
    $json = json_decode($string, true);
    
    echo $json["imgblobs"][0]["candidate"]["text"];
?>